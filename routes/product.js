const router = require("express").Router();
const { product } = require("../controllers");

// Get Data Product
router.get("/", product.getDataProduct);

// Get Data Product With :id
router.get("/api/getProduct/:id", product.getDataProductByID);

// Add Data Product
router.post("/api/addProduct", product.addDataProduct);

// Edit Data Product
router.put("/api/updateProduct/:id", product.updateDataProduct);

// Delete data Product
router.delete("/api/deleteProduct/:id", product.deleteDataProduct);

module.exports = router;
