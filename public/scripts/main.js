// Add Data
function addData() {
  const data = {
    name: document.getElementById("nameProduct").value,
    price: document.getElementById("priceProduct").value,
    discount: document.getElementById("discountProduct").value,
    stock: document.getElementById("stockProduct").value,
    sold: "0",
  };
  let res = axios.post("http://localhost:3000/api/addProduct", data);
  window.location.href = "http://localhost:3000/";
}

//Delete Data
function deleteData(idProduct, nameProduct) {
  const confirmAction = confirm("Are you sure to delete data " + nameProduct);
  if (confirmAction) {
    let res = axios.delete(
      "http://localhost:3000/api/deleteProduct/" + idProduct
    );
    window.location.href = "http://localhost:3000/";
  }
}

// Update Data
function updateData(idProduct) {
  const data = {
    name: document.getElementById("nameProductU" + idProduct).value,
    price: document.getElementById("priceProductU" + idProduct).value,
    discount: document.getElementById("discountProductU" + idProduct).value,
    stock: document.getElementById("stockProductU" + idProduct).value,
    sold: document.getElementById("soldProductU" + idProduct).value,
  };
  let res = axios.put(
    "http://localhost:3000/api/updateProduct/" + idProduct,
    data
  );
  window.location.href = "http://localhost:3000/";
}
