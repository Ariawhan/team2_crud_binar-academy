Project to learn CRUD with MySQL database in node.js with API. 

```
Jobdesk : - Ariawan -> Create Data with INSERT query
          - Yuneda -> Get data With SELECT query
          - Maria -> Get data With SELECT and filter with WHERE query
          - Yudha -> Update data With Update query
          - Aulia -> DELETE data with DELETE query
```


This simple views with EJS module and bootstrap
![Alt-Text](/public/view.jpg)

 
Copyright : Team 2 ([Ariawan](https://gitlab.com/Ariawhan), [Yuneda](https://gitlab.com/yuneda), [Yudha](https://gitlab.com/pratamaramadhan08), [Maria](https://gitlab.com/mnatalia283), [Aulia](https://gitlab.com/Aulia_utami))
