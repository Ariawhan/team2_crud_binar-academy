const config = require("../lib/database");
const mysql = require("mysql");
const { application } = require("express");
const pool = mysql.createPool(config);

pool.on("error", (err) => {
  if (err) throw err;
  console.log("connect");
});

module.exports = {
  //Get Data Product
  getDataProduct(req, res) {
    pool.getConnection(function (err, connection) {
      if (err) throw err;
      connection.query(`SELECT * FROM product`, function (error, results) {
        if (error) throw error;
        console.log(results);
        res.render("index.ejs", {
          data: results,
          total: results.length,
          success: true,
          message: "Berhasil ambil data!",
        });
      });
      connection.release();
    });
  },
  // Get Data Product With :id
  getDataProductByID(req, res) {
    let id = req.params.id;
    pool.getConnectionById(function (req, connection) {
      connection.query(
        `SELECT * FROM product WHERE id=` + id,
        function (error, results) {
          if (error) throw error;
          console.log(results);
          res.render("index.ejs", {
            data: results,
            success: true,
            message: "Berhasil ambil data!",
          });
        }
      );
      connection.release();
    });
  },
  // Add Data Product
  addDataProduct(req, res) {
    let data = {
      name: req.body.name,
      price: req.body.price,
      discount: req.body.discount,
      stock: req.body.stock,
      sold: req.body.sold,
    };
    console.log(req.body);

    pool.getConnection(function (err, connection) {
      if (err) throw err;
      connection.query(
        `
            INSERT INTO product SET ?;
            `,
        data,
        function (error, results) {
          if (error) throw error;
          res.send({
            success: true,
            message: "Berhasil tambah data!",
          });
        }
      );
      connection.release();
    });
  },
  // Edit Data Product
  updateDataProduct(req, res) {
    let data = {
      name: req.body.name,
      price: req.body.price,
      discount: req.body.discount,
      stock: req.body.stock,
      sold: req.body.sold,
    };
    let id = req.params.id;
    console.log(req.body);
    console.log(req.params.id);

    pool.getConnection(function (err, connection) {
      if (err) throw err;
      console.log(err);
      connection.query(
        "UPDATE product SET ? WHERE id = " + id,
        data,
        function (error, results) {
          if (error) throw error;
          console.log(error);
          res.send({
            success: true,
            message: "Berhasil update data!",
          });
        }
      );
      connection.release();
    });
  },

  // Delete data Product
  deleteDataProduct(req, res) {
    let id = req.params.id;
    console.log(req.params);
    pool.getConnection(function (err, connection) {
      if (err) throw err;
      console.log(err);
      connection.query(
        "DELETE FROM product WHERE id = " + id,
        function (error, results) {
          if (error) throw error;
          console.log(error);
          res.send({
            success: true,
            message: "Berhasil delete data!",
          });
        }
      );
      connection.release();
    });
  },
};
