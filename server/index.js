const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const appRoute = require("../routes/product.js");

app.set("view engine", "ejs");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static("public"));

app.use("/", appRoute);

app.listen(3000);
